﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpawnCoins : MonoBehaviour {

    public GameObject[] coins;
    private GameObject go;
    private Quaternion rot = Quaternion.Euler(90, 0, 0);

    void OnEnable()
    {
        spawnCoin();
    }

    void OnDisable()
    {
        Destroy(go);
    }

    void Update()
    {

    }

    void spawnCoin()
    {
        if (Random.Range(0, 5) == 2)
        {
            int spawn = Random.Range(1, 4);

            if (spawn == 1)
            {
                go = (GameObject)Instantiate(coins[0], transform.position, rot);
            }
            else if (spawn == 2)
            {
                go = (GameObject)Instantiate(coins[1], transform.position, rot);
            }
            else if (spawn == 3)
            {
                go = (GameObject)Instantiate(coins[2], transform.position, rot);
            }
            go.transform.SetParent(this.gameObject.transform);
        }
    }
}