﻿using UnityEngine;
using System.Collections;

public class Coins : MonoBehaviour {

    private GameObject gameController;
    void Start() {
        gameController = GameObject.FindGameObjectWithTag("GameController");
    }

    void Update() {

    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Builder" && col.gameObject.GetComponent<CharacterSelect>().markerSet == 1)
        {
             if (tag == "bCoin")
             {
                 gameController.GetComponent<ScoreManagingScript>().mulitiplier += 1;
                 gameController.GetComponent<ScoreManagingScript>().AddPoints(10);
                 gameController.GetComponent<coinCount>().Bscore += 1;
                this.gameObject.SetActive(false);
                 //Destroy(this.gameObject);
             }
             else if(tag == "tCoin" || this.gameObject.tag == "hCoin")
             {
                 gameController.GetComponent<ScoreManagingScript>().mulitiplier = 1;
             }
        }
        if(col.gameObject.tag == "Hacker" && col.gameObject.GetComponent<CharacterSelect>().markerSet == 1)
        {
            if (tag == "hCoin")
            {
                gameController.GetComponent<ScoreManagingScript>().mulitiplier += 1;
                gameController.GetComponent<ScoreManagingScript>().AddPoints(10);
                gameController.GetComponent<coinCount>().Hscore += 1;
                this.gameObject.SetActive(false);
                //Destroy(this.gameObject);
            }
            else if (tag == "tCoin" || tag == "bCoin")
            {
                gameController.GetComponent<ScoreManagingScript>().mulitiplier = 1;
            }
        }
        if(col.gameObject.tag == "Tank" && col.gameObject.GetComponent<CharacterSelect>().markerSet == 1)
        {
            if (tag == "tCoin")
            {
                gameController.GetComponent<ScoreManagingScript>().mulitiplier += 1;
                gameController.GetComponent<ScoreManagingScript>().AddPoints(10);
                gameController.GetComponent<coinCount>().Tscore += 1;
                this.gameObject.SetActive(false);
                //Destroy(this.gameObject);
            }
            else if (tag == "bCoin" || tag == "hCoin")
            {
                gameController.GetComponent<ScoreManagingScript>().mulitiplier = 1;
            }
        }
    }
}