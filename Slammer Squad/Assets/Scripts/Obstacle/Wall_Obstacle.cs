﻿using UnityEngine;
using System.Collections;

public class Wall_Obstacle : MonoBehaviour {

    public GameObject main;
    public ParticleSystem particle;
    private AudioSource aud;
    private GameObject gameController;

    void OnEnable()
    {
        aud = GameObject.FindGameObjectWithTag("WallSound").GetComponent<AudioSource>();
        gameController = GameObject.FindGameObjectWithTag("GameController");
        main.SetActive(true);
        this.gameObject.SetActive(true);
    }

    void OnTriggerEnter(Collider col)
    {
        // Detector
        if (col.gameObject.name == "Detector")
        {
            gameController.GetComponent<UI>().incDoor.enabled = true;
        }
        // Wall obstacle collision
        if (col.gameObject.tag == "Tank" && col.gameObject.GetComponent<CharacterSelect>().markerSet == 1)
        {
            gameController.GetComponent<ScoreManagingScript>().AddPoints(100);
            //Destroy(main);
            main.SetActive(false);
            particle.Play();
            aud.Play();
            //Destroy(this.gameObject);
            this.gameObject.SetActive(false);
        }
        else if (col.gameObject.tag == "Hacker" || col.gameObject.tag == "Builder" && col.gameObject.GetComponent<CharacterSelect>().markerSet != 1)
        {
            gameController.GetComponent<ScoreManagingScript>().paused = true;
            Destroy(this.gameObject);
        }
    }
}