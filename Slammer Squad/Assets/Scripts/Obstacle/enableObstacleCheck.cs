﻿using UnityEngine;
using System.Collections;

public class enableObstacleCheck : MonoBehaviour {

    public GameObject Check;

    void OnEnable()
    {
        Check.SetActive(true);
    }

    void OnDisable()
    {
        Check.SetActive(false);
    }
}