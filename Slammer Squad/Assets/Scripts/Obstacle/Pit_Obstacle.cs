﻿using UnityEngine;
using System.Collections;

public class Pit_Obstacle : MonoBehaviour {
    
    public GameObject main;
    public ParticleSystem particle;
    public GameObject L;
    public GameObject R;
    private AudioSource aud;
    private GameObject gameController;
    private GameObject char1;
    private GameObject char2;
    private GameObject char3;
    private GameObject gate;

    void OnEnable()
    {
        aud = GameObject.FindGameObjectWithTag("PitSound").GetComponent<AudioSource>();
        main.SetActive(false);
        L.SetActive(false);
        R.SetActive(false);

        gameController = GameObject.FindGameObjectWithTag("GameController");
        char1 = GameObject.FindGameObjectWithTag("Builder");
        char2 = GameObject.FindGameObjectWithTag("Hacker");
        char3 = GameObject.FindGameObjectWithTag("Tank");
    }

    void OnTriggerEnter(Collider col)
    {
        // Detector
        if (col.gameObject.name == "Detector")
        {
            gameController.GetComponent<UI>().incPit.enabled = true;
        }
        // Pit obstacle collision
        if (col.gameObject.tag == "Builder" && col.gameObject.GetComponent<CharacterSelect>().markerSet == 1)
        {
            gameController.GetComponent<ScoreManagingScript>().AddPoints(100);
            main.SetActive(true);
            particle.Play();
            aud.Play();
            //Destroy(this.gameObject);
            this.gameObject.SetActive(false);
        }
        else if (col.gameObject.tag == "Hacker" || col.gameObject.tag == "Tank" && col.gameObject.GetComponent<CharacterSelect>().markerSet == 1)
        {
            gameController.GetComponent<ScoreManagingScript>().paused = true;
            char1.GetComponent<Rigidbody>().useGravity = true;
            char1.transform.parent = null;
            char1.GetComponent<Collider>().isTrigger = false;
            char2.GetComponent<Rigidbody>().useGravity = true;
            char2.transform.parent = null;
            char2.GetComponent<Collider>().isTrigger = false;
            if (char3.GetComponent<CharacterSelect>().tutorial == false)
            {
                char3.GetComponent<Rigidbody>().useGravity = true;
            }
            char3.transform.parent = null;
            char3.GetComponent<Collider>().isTrigger = false;
            L.SetActive(true);
            R.SetActive(true);
            Destroy(this.gameObject);
        }
    }
}