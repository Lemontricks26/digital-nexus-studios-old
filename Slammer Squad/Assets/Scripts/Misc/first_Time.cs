﻿using UnityEngine;
using System.Collections;

public class first_Time : MonoBehaviour {
    
    void Start() {
    }

    void Update() {

    }

    public int getHasPlayed()
    {
        return PlayerPrefs.GetInt("hasPlayed");
    }

    // 0 for tutorial, 1 for no tutorial
    public void setHasPlayed(int i)
    {
        PlayerPrefs.SetInt("hasPlayed", i);
        PlayerPrefs.Save();
    }

    void onDestroy()
    {
        PlayerPrefs.Save();
    }
}
