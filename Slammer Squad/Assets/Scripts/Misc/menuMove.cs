﻿using UnityEngine;
using System.Collections;

public class menuMove : MonoBehaviour {
    
    public GameObject camPoint2;
    public GameObject canvas;
    public int deep;
    public float speed;

	void Start () {
        deep = 0;
	}
	
	void Update () {
	    switch (deep)
        {
            case 1:
                transform.position = Vector3.MoveTowards(transform.position, camPoint2.transform.position, speed);
                if(transform.position == camPoint2.transform.position)
                {
                    canvas.SetActive(true);
                    deep = 2;
                }
                break;
        }
	}
}
