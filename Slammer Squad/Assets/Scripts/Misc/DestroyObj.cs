﻿using UnityEngine;
using System.Collections;

public class DestroyObj : MonoBehaviour {

    public GameObject obj;
    public GameObject inGameUI;
    public GameObject gameManager;
	public GameObject guard;

	void Start () {
        inGameUI.SetActive(false);
    }
	
	void Update () {
	
	}

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Tank" || col.gameObject.tag == "Builder" || col.gameObject.tag == "Hacker")
        {
            gameManager.GetComponent<ScoreManagingScript>().paused = false;
            gameManager.GetComponent<first_Time>().setHasPlayed(1);
            inGameUI.SetActive(true);
            Destroy(obj);
            guard.SetActive(true);
            Destroy(this.gameObject);
        }
    }
}
