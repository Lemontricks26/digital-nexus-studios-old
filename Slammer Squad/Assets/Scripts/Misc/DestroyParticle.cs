﻿using UnityEngine;
using System.Collections;

public class DestroyParticle : MonoBehaviour {
    
	void Start () {
	
	}
	
	void Update () {
	    if(this.gameObject.GetComponent<ParticleSystem>().IsAlive() == false)
        {
            Destroy(this.gameObject);
        }
	}
}
