﻿using UnityEngine;
using System.Collections;

public class TankAnimation : MonoBehaviour {

    Animator anim;
    public GameObject cam;

    void Awake()
    {
        anim = GetComponent<Animator>();
    }
    

	void Start () {

	}
	
	void Update () {
	
	}

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "WallAnimation")
        {
            anim.SetBool("ShoulderCharge", true);
            cam.GetComponent<Animator>().SetBool("ShakeItGood", true);
        }
        else
        {
            anim.SetBool("ShoulderCharge", false);
            cam.GetComponent<Animator>().SetBool("ShakeItGood", false);
        }
    }
}