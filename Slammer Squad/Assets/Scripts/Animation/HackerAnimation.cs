﻿using UnityEngine;
using System.Collections;

public class HackerAnimation : MonoBehaviour {

    Animator anim;

    void Awake()
    {
        anim = GetComponent<Animator>();
    }
    void Start () {
	
	}
	
	void Update () {
	
	}

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "DoorAnimation")
        {
            // Play ani
            anim.SetBool("Hack", true);
        }
        else
        {
            anim.SetBool("Hack", false);
        }
    }
}
