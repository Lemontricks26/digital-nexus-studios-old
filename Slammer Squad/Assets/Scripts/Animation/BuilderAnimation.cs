﻿using UnityEngine;
using System.Collections;

public class BuilderAnimation : MonoBehaviour {

    Animator anim;

    void Awake()
    {
        anim = GetComponent<Animator>();
    }
	void Start () {
	
	}
	
	void Update () {

    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "PitAnimation")
        {
            anim.SetBool("Ladderthrow", true);
        }
        else
        {
            anim.SetBool("Ladderthrow", false);
        }
    }
}
