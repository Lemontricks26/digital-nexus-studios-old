﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ScoreManagingScript : MonoBehaviour
{
    //sets current score to 0
    public float score = 0;
    private int Bscore = 0;
    public int highscore;
    // Create text for this
    public int mulitiplier = 1;

    public bool paused = true;

    private Vector3 start;
    private int distance;
    private float dist;
    private int time;
    private int startTime;
    [SerializeField]
    private int minutes;
    [SerializeField]
    private int seconds;

    [SerializeField]
    Text scoring;
    [SerializeField]
    Text HiScoreNumber;
    [SerializeField]
    Text Multiplier;
    [SerializeField]
    Text lastScore;
    [SerializeField]
    Text lastScore_two;
    [SerializeField]
    Text HiScoreNumber_two;
    [SerializeField]
    Text clock;
    [SerializeField]
    Text distanceText;
    [SerializeField]
    GameObject Characters;

    void Awake()
    {
        startTime = 0;
    }

    void Start()
    {
        start = Characters.transform.position;

        score = 0;
        lastScore.GetComponent<Text>().text = score.ToString();
        lastScore_two.GetComponent<Text>().text = score.ToString();
        scoring.GetComponent<Text>().text = score.ToString();

        highscore = PlayerPrefs.GetInt("highscore", highscore);
        HiScoreNumber.text = highscore.ToString();
        HiScoreNumber_two.text = highscore.ToString();
    }

    void FixedUpdate()
    {
        if (!paused)
        {
            time = (int)Time.time - startTime;

            score = score + (1 * Mathf.Clamp(mulitiplier, 1, 15) * Time.deltaTime);
            Bscore = (int)score;
            //The score will be set as the string

            scoring.text = Bscore.ToString();
            lastScore.text = Bscore.ToString();
            lastScore_two.text = Bscore.ToString();
            Multiplier.GetComponent<Text>().text = "Multiplier X " + mulitiplier.ToString();
        }
        if (score > highscore)
        {
            highscore = (int)score;
            HiScoreNumber.text = highscore.ToString();
            HiScoreNumber_two.text = highscore.ToString();
            PlayerPrefs.SetInt("highscore", highscore);
        }

        dist = Vector3.Distance(start, Characters.transform.position);
        distance = (int)dist;
        distanceText.text = distance.ToString();

        minutes = time / 60;
        seconds = time % 60;
        clock.text = minutes + ":" + seconds;
    }

    public void AddPoints(int pointsAdd)
    {
        if (!paused)
        {
            score += pointsAdd;
        }
    }

    public void Reset()
    {
        score = 0;
    }

    void OnDestroy()
    {
        PlayerPrefs.SetInt("highscore", highscore);
        PlayerPrefs.Save();
    }

}