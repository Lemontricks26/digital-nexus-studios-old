﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UI : MonoBehaviour {

	public Image incDoor;
	public Image incWall;
	public Image incPit;

	[Range(0f,1.0f)]
	public float flashTime;
	void Start () {
        incDoor.enabled = false;
        incWall.enabled = false;
        incPit.enabled = false;
    }
	
	void Update () {
	    if(incDoor.enabled == true)
        {
			Invoke("disDoor", flashTime);
        }
        if (incWall.enabled == true)
        {
			Invoke("disWall", flashTime);
        }
        if (incPit.enabled == true)
        {
			Invoke("disPit", flashTime);
        }
    }

    void disDoor()
    {
        incDoor.enabled = false;
    }
    void disWall()
    {
        incWall.enabled = false;
    }
    void disPit()
    {
        incPit.enabled = false;
    }
}
