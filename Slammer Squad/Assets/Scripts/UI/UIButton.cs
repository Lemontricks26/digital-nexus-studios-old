﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIButton : MonoBehaviour {

    private bool paused = false;
    private bool once = false;

    [SerializeField]
    CharacterSelect C1;
    [SerializeField]
    CharacterSelect C2;
    [SerializeField]
    CharacterSelect C3;

    [SerializeField]
    Camera deep;

    [SerializeField]
    GameObject highScoreCanvas;
    [SerializeField]
    GameObject menuCanvas;
    [SerializeField]
    GameObject pauseCanvas;
    [SerializeField]
    GameObject gameOverCanvas;
    [SerializeField]
    GameObject inGameUI;
    [SerializeField]
    GameObject creditCanvas;
    [SerializeField]
    GameObject pauseCan;

	void Start () {

        inGameUI.SetActive(false);
        highScoreCanvas.SetActive(false);
        menuCanvas.SetActive(false);
        pauseCanvas.SetActive(false);
        gameOverCanvas.SetActive(false);
        creditCanvas.SetActive(false);
    }
	
	void Update () {
        if (C1 != null)
        {
            if (C1.Alive == false && once == false || C2.Alive == false && once == false || C3.Alive == false && once == false)
            {
                GetComponent<ScoreManagingScript>().paused = true;
                once = true;
                inGameUI.SetActive(false);
                pauseCan.SetActive(false);
                gameOverCanvas.SetActive(true);
            }
        }
	}

    public void returnMenu()
    {
        SceneManager.LoadScene("menu");
    }

    public void ChangeToMain()
    {
        SceneManager.LoadScene("main");
    }

    public void pause()
    {
        if (paused)
        {
            paused = false;
            Time.timeScale = 1;
            GetComponent<Speed>().paused = false;
            GetComponent<ScoreManagingScript>().paused = false;
            C1.paused = false;
            C2.paused = false;
			C3.paused = false;
			pauseCanvas.SetActive(false);
        }
        else if (!paused)
        {
            paused = true;
            Time.timeScale = 0;
            GetComponent<Speed>().paused = true;
            GetComponent<ScoreManagingScript>().paused = true;
            C1.paused = true;
            C2.paused = true;
			C3.paused = true;
			pauseCanvas.SetActive(true);
        }
    }

    public void highscore()
    {
        highScoreCanvas.SetActive(true);
    }

    public void restart()
    {
        SceneManager.LoadScene("main");
    }

    public void MenuCanvasfromcredits()
    {
        menuCanvas.SetActive(true);
        creditCanvas.SetActive(false);
    }

    public void GameOver()
    {
        deep.GetComponent<menuMove>().deep = 1;
        gameOverCanvas.SetActive(false);
    }
    public void CreditsCanvas()
    {
        menuCanvas.SetActive(false);
        creditCanvas.SetActive(true);
    }
}
