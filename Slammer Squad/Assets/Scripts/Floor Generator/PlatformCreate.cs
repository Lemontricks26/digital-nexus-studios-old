﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlatformCreate : MonoBehaviour
{
    private FloorGen lvlGen;
    private GameObject Player;

    void Start()
    {
        lvlGen = GameObject.FindGameObjectWithTag("lvlGen").GetComponent<FloorGen>();
        Player = GameObject.FindGameObjectWithTag("Marker1");
    }
    
    void Update()
    {
        if (transform.position.x < Player.transform.position.x - 27)
        {
            lvlGen.floorSelect();
            this.gameObject.SetActive(false);
            //Destroy(gameObject);
        }
    }
}
