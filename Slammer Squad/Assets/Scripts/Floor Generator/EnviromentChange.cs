﻿using UnityEngine;
using System.Collections;

public class EnviromentChange : MonoBehaviour {

    public FloorGen indicator;
    public int counter = 0;
    private bool doOnce = false;
    private int EnviroChange;

    void Awake()
    {
        Application.targetFrameRate = 60;
    }

    void Start ()
    {
        EnviroChange = Random.Range(10, 20);
    }
	
	void Update () {
        if (doOnce == true)
        {
            NewZone();
        }
        if(counter == EnviroChange)
        {
            EnviroChange = Random.Range(10, 20);
            doOnce = true;
        }
	}

    void NewZone()
    {
        if(indicator.direction == 1 && indicator.currentZone == 2)
        {
            indicator.direction = 2;
        }
        else if(indicator.direction == 2 && indicator.currentZone == 3)
        {
            indicator.direction = 1;
        }
        else
        {
            indicator.direction = Random.Range(1, 3);
        }
        indicator.doTransition = true;
        doOnce = false;
        counter = 0;
    }
}