﻿using UnityEngine;
using System.Collections;

public class BuildingSpawner : MonoBehaviour {

    public GameObject[] buildings;
    GameObject newBuild;

    void Start () {
        int num = Random.Range(1, 8);
        Vector3 spawnPos = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        Quaternion rotation = Quaternion.Euler(0, 180, 0);

        switch(num)
        {
            case 1:
                newBuild = (GameObject)Instantiate(buildings[0], spawnPos, rotation);
                newBuild.transform.SetParent(transform);
                break;
            case 2:
                newBuild = (GameObject)Instantiate(buildings[1], spawnPos, rotation);
                newBuild.transform.SetParent(transform);
                break;
            case 3:
                newBuild = (GameObject)Instantiate(buildings[2], spawnPos, rotation);
                newBuild.transform.SetParent(transform);
                break;
            case 4:
                newBuild = (GameObject)Instantiate(buildings[3], spawnPos, rotation);
                newBuild.transform.SetParent(transform);
                break;
            case 5:
                newBuild = (GameObject)Instantiate(buildings[4], spawnPos, rotation);
                newBuild.transform.SetParent(transform);
                break;
            case 6:
                newBuild = (GameObject)Instantiate(buildings[5], spawnPos, rotation);
                newBuild.transform.SetParent(transform);
                break;
            case 7:
                newBuild = (GameObject)Instantiate(buildings[6], spawnPos, rotation);
                newBuild.transform.SetParent(transform);
                break;
            case 8:
                newBuild = (GameObject)Instantiate(buildings[7], spawnPos, rotation);
                newBuild.transform.SetParent(transform);
                break;
        }
	
	}
}
