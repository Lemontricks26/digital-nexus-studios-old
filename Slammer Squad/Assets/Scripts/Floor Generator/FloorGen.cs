﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FloorGen : MonoBehaviour {

    public GameObject startArea;
    public GameObject[] Prison; // 1
    public GameObject[] RoofTop; // 2
    public GameObject[] Sewers; // 3
    public GameObject[] transitions;

    public GameObject Characters;
    public GameObject gameManager;
    public int currentZone = 1;
    public int direction = 0; // 1 for Up, 2 for Down
    public bool doTransition = false;

    private int tileType = 0; // 0 for Path, 1 for Obstacle
    private bool doTransitionTwo = false;
    private bool once = true;
    private Vector3 blockPosition;
    private Quaternion rotation = Quaternion.Euler(0, 0, 0);
    private Quaternion rotation2 = Quaternion.Euler(0, 180, 0);

    List<GameObject> prisonPaths;
    List<GameObject> prisonObstacles;
    List<GameObject> roofPaths;
    List<GameObject> roofObstacles;
    List<GameObject> sewerPaths;
    List<GameObject> sewerObstacles;
    List<GameObject> transitionPaths;

    void Start()
    {
        prisonPaths = new List<GameObject>();
        prisonObstacles = new List<GameObject>();
        roofPaths = new List<GameObject>();
        roofObstacles = new List<GameObject>();
        sewerPaths = new List<GameObject>();
        sewerObstacles = new List<GameObject>();
        transitionPaths = new List<GameObject>();

        // Object pool transition
        for(int i = 0; i < 7; i++)
        {
            GameObject obj = (GameObject)Instantiate(transitions[i]);
            obj.SetActive(false);
            transitionPaths.Add(obj);
        }

        // Object pool prison paths
        for (int i = 0; i < 5; i++)
        {
            GameObject obj = (GameObject)Instantiate(Prison[i + 3]);
            obj.SetActive(false);
            prisonPaths.Add(obj);
        }
        for (int i = 0; i < 5; i++)
        {
            GameObject obj = (GameObject)Instantiate(Prison[i + 3]);
            obj.SetActive(false);
            prisonPaths.Add(obj);
        }

        // Object pool roof paths
        for(int i = 0; i < 5; i++)
        {
            GameObject obj = (GameObject)Instantiate(RoofTop[i + 3]);
            obj.SetActive(false);
            roofPaths.Add(obj);
        }
        for (int i = 0; i < 5; i++)
        {
            GameObject obj = (GameObject)Instantiate(RoofTop[i + 3]);
            obj.SetActive(false);
            roofPaths.Add(obj);
        }

        // Object pool sewer paths
        for (int i = 0; i < 4; i++)
        {
            GameObject obj = (GameObject)Instantiate(Sewers[i + 3]);
            obj.SetActive(false);
            sewerPaths.Add(obj);
        }
        for (int i = 0; i < 4; i++)
        {
            GameObject obj = (GameObject)Instantiate(Sewers[i + 3]);
            obj.SetActive(false);
            sewerPaths.Add(obj);
        }

        // Object pool prison obstacles
        for (int i = 0; i < 2; i++)
        {
            GameObject obj = (GameObject)Instantiate(Prison[0]);
            obj.SetActive(false);
            prisonObstacles.Add(obj);
        }
        for (int i = 0; i < 2; i++)
        {
            GameObject obj = (GameObject)Instantiate(Prison[1]);
            obj.SetActive(false);
            prisonObstacles.Add(obj);
        }
        for (int i = 0; i < 2; i++)
        {
            GameObject obj = (GameObject)Instantiate(Prison[2]);
            obj.SetActive(false);
            prisonObstacles.Add(obj);
        }

        // Object pool roof obstacles
        for (int i = 0; i < 2; i++)
        {
            GameObject obj = (GameObject)Instantiate(RoofTop[0]);
            obj.SetActive(false);
            roofObstacles.Add(obj);
        }
        for (int i = 0; i < 2; i++)
        {
            GameObject obj = (GameObject)Instantiate(RoofTop[1]);
            obj.SetActive(false);
            roofObstacles.Add(obj);
        }
        for (int i = 0; i < 2; i++)
        {
            GameObject obj = (GameObject)Instantiate(RoofTop[2]);
            obj.SetActive(false);
            roofObstacles.Add(obj);
        }

        // Object pool sewer obstacles
        for (int i = 0; i < 2; i++)
        {
            GameObject obj = (GameObject)Instantiate(Sewers[0]);
            obj.SetActive(false);
            sewerObstacles.Add(obj);
        }
        for (int i = 0; i < 2; i++)
        {
            GameObject obj = (GameObject)Instantiate(Sewers[1]);
            obj.SetActive(false);
            sewerObstacles.Add(obj);
        }
        for (int i = 0; i < 2; i++)
        {
            GameObject obj = (GameObject)Instantiate(Sewers[2]);
            obj.SetActive(false);
            sewerObstacles.Add(obj);
        }

        direction = Random.Range(1, 2);

        for (int i = 0; i <= 36; i += 12)
        {
            if (once == true)
            {
                blockPosition = new Vector3(i, transform.position.y, transform.position.z);
                blockPosition = new Vector3((int)blockPosition.x, (int)blockPosition.y, (int)blockPosition.z);
                once = false;
            }
            for (int p = 0; p < prisonPaths.Count; p++)
            {
                if (!prisonPaths[p].activeInHierarchy)
                {
                    prisonPaths[p].transform.position = blockPosition;
                    prisonPaths[p].transform.rotation = rotation;
                    prisonPaths[p].SetActive(true);
                    blockPosition.x += 12;
                    tileType = 0;
                    break;
                }
            }
            blockPosition = new Vector3((int)blockPosition.x, (int)blockPosition.y, (int)blockPosition.z);
        }
    }

    void Update()
    {
#if UNITY_EDITOR
        if (Input.GetKeyDown("w"))
        {
            doTransition = true;
            gameManager.GetComponent<EnviromentChange>().counter = 0;
            direction = 1;
        }
        if (Input.GetKeyDown("s"))
        {
            doTransition = true;
            gameManager.GetComponent<EnviromentChange>().counter = 0;
            direction = 2;
        }
#endif
    }

    int prisonTileNum()
    {
        int num = 0;

        if (tileType == 1) // If last tile was a obstacle then create a path
        {
            num = Random.Range(4, 8);
        }
        else if (tileType == 0) // If last tile was a path then create a path or obstacle
        {
            num = Random.Range(1, 8);
        }

        return num;
    }

    int roofTileNum()
    {
        int num = 0;

        if (tileType == 1) // If last tile was a obstacle then create a path
        {
            num = Random.Range(4, 8);
        }
        else if (tileType == 0) // If last tile was a path then create a path or obstacle
        {
            num = Random.Range(1, 8);
        }

        return num;
    }

    int sewerTileNum()
    {
        int num = 0;

        if (tileType == 1) // If last tile was a obstacle then create a path
        {
            num = Random.Range(4, 8);
        }
        else if (tileType == 0) // If last tile was a path then create a path or obstacle
        {
            num = Random.Range(1, 8);
        }

        return num;
    }

    void changeZone()
    {
        if (currentZone == 1 && direction == 1)
        {
            transitionPaths[0].transform.position = blockPosition;
            transitionPaths[0].transform.rotation = rotation;
            transitionPaths[0].SetActive(true);
            blockPosition.x += 12;
            // Prison Go to Roof
        }
        else if (currentZone == 1 && direction == 2)
        {
            transitionPaths[4].transform.position = blockPosition;
            transitionPaths[4].transform.rotation = rotation;
            transitionPaths[4].SetActive(true);
            blockPosition.x += 12;
            // Prison Go to Sewers
        }
        else if (currentZone == 2 && direction == 1)
        {
            for(int i = 0; i < roofPaths.Count; i++)
            {
                if(!roofPaths[i].activeInHierarchy)
                {
                    roofPaths[i].transform.position = blockPosition;
                    roofPaths[i].transform.rotation = transform.rotation;
                    roofPaths[i].SetActive(true);
                    break;
                }
            }
            blockPosition.x += 12;
            // Roof Go to Roof
        }
        else if (currentZone == 2 && direction == 2)
        {
            transitionPaths[2].transform.position = blockPosition;
            transitionPaths[2].transform.rotation = rotation2;
            transitionPaths[2].SetActive(true);
            blockPosition.x += 12;
            // Roof Go to Prison
        }
        else if (currentZone == 3 && direction == 1)
        {
            transitionPaths[6].transform.position = blockPosition;
            transitionPaths[6].transform.rotation = rotation;
            transitionPaths[6].SetActive(true);
            blockPosition.x += 12;
            // Sewers Go to Prison
        }
        else if (currentZone == 3 && direction == 2)
        {
            for (int i = 0; i < sewerPaths.Count; i++)
            {
                if (!sewerPaths[i].activeInHierarchy)
                {
                    sewerPaths[i].transform.position = blockPosition;
                    sewerPaths[i].transform.rotation = transform.rotation;
                    sewerPaths[i].SetActive(true);
                    break;
                }
            }
            blockPosition.x += 12;
            // Sewers Go to Sewers
        }
        blockPosition = new Vector3((int)blockPosition.x, (int)blockPosition.y, (int)blockPosition.z);
        tileType = 1;
        doTransition = false;
        doTransitionTwo = true;
    }

    void changeZoneTwo()
    {
        if (currentZone == 1 && direction == 1)
        {
            transitionPaths[1].transform.position = blockPosition;
            transitionPaths[1].transform.rotation = rotation;
            transitionPaths[1].SetActive(true);
            blockPosition.x += 12;
            currentZone = 2;
            // Prison Go to Roof
        }
        else if (currentZone == 1 && direction == 2)
        {
            transitionPaths[5].transform.position = blockPosition;
            transitionPaths[5].transform.rotation = rotation;
            transitionPaths[5].SetActive(true);
            blockPosition.x += 12;
            currentZone = 3;
            // Prison Go to Sewers
        }
        else if (currentZone == 2 && direction == 1)
        {
            for (int i = 0; i < roofPaths.Count; i++)
            {
                if (!roofPaths[i].activeInHierarchy)
                {
                    roofPaths[i].transform.position = blockPosition;
                    roofPaths[i].transform.rotation = transform.rotation;
                    roofPaths[i].SetActive(true);
                    break;
                }
            }
            blockPosition.x += 12;
            // Roof Go to Roof
        }
        else if (currentZone == 2 && direction == 2)
        {
            transitionPaths[3].transform.position = blockPosition;
            transitionPaths[3].transform.rotation = rotation;
            transitionPaths[3].SetActive(true);
            blockPosition.x += 12;
            currentZone = 1;
            // Roof Go to Prison
        }
        else if (currentZone == 3 && direction == 1)
        {
            transitionPaths[7].transform.position = blockPosition;
            transitionPaths[7].transform.rotation = rotation;
            transitionPaths[7].SetActive(true);
            blockPosition.x += 12;
            currentZone = 1;
            // Sewers Go to Prison
        }
        else if (currentZone == 3 && direction == 2)
        {
            for (int i = 0; i < sewerPaths.Count; i++)
            {
                if (!sewerPaths[i].activeInHierarchy)
                {
                    sewerPaths[i].transform.position = blockPosition;
                    sewerPaths[i].transform.rotation = transform.rotation;
                    sewerPaths[i].SetActive(true);
                    break;
                }
            }
            blockPosition.x += 12;
            // Sewers Go to Sewers
        }
        blockPosition = new Vector3((int)blockPosition.x, (int)blockPosition.y, (int)blockPosition.z);
        tileType = 1;
        doTransitionTwo = false;
    }

    public void floorSelect()
    {
        if (doTransitionTwo == true)
        {
            changeZoneTwo();
        }
        else if (doTransition == true)
        {
            changeZone();
        }
        else
        {
            switch (currentZone)
            {
                case 1:
                    PrisonZone();
                    break;
                case 2:
                    RoofZone();
                    break;
                case 3:
                    SewerZone();
                    break;
            }
        }
    }

    void PrisonZone()
    {
        switch (prisonTileNum())
        {
            case 1:
                // Door
                if(!prisonObstacles[0].activeInHierarchy)
                {
                    prisonObstacles[0].transform.position = blockPosition;
                    prisonObstacles[0].transform.rotation = rotation;
                    prisonObstacles[0].SetActive(true);
                    blockPosition.x += 12;
                    tileType = 1;
                    break;
                }
                else if(!prisonObstacles[1].activeInHierarchy)
                {
                    prisonObstacles[1].transform.position = blockPosition;
                    prisonObstacles[1].transform.rotation = rotation;
                    prisonObstacles[1].SetActive(true);
                    blockPosition.x += 12;
                    tileType = 1;
                    break;
                }
                break;
            case 2:
                // Pit
                if (!prisonObstacles[2].activeInHierarchy)
                {
                    prisonObstacles[2].transform.position = blockPosition;
                    prisonObstacles[2].transform.rotation = rotation;
                    prisonObstacles[2].SetActive(true);
                    blockPosition.x += 12;
                    tileType = 1;
                    break;
                }
                else if (!prisonObstacles[3].activeInHierarchy)
                {
                    prisonObstacles[3].transform.position = blockPosition;
                    prisonObstacles[3].transform.rotation = rotation;
                    prisonObstacles[3].SetActive(true);
                    blockPosition.x += 12;
                    tileType = 1;
                    break;
                }
                break;
            case 3:
                // Wall
                if (!prisonObstacles[4].activeInHierarchy)
                {
                    prisonObstacles[4].transform.position = blockPosition;
                    prisonObstacles[4].transform.rotation = rotation;
                    prisonObstacles[4].SetActive(true);
                    blockPosition.x += 12;
                    tileType = 1;
                    break;
                }
                else if (!prisonObstacles[5].activeInHierarchy)
                {
                    prisonObstacles[5].transform.position = blockPosition;
                    prisonObstacles[5].transform.rotation = rotation;
                    prisonObstacles[5].SetActive(true);
                    blockPosition.x += 12;
                    tileType = 1;
                    break;
                }
                break;
            case 4:
                // Path
                for (int i = 0; i < prisonPaths.Count; i++)
                {
                    if (!prisonPaths[i].activeInHierarchy)
                    {
                        prisonPaths[i].transform.position = blockPosition;
                        prisonPaths[i].transform.rotation = rotation;
                        prisonPaths[i].SetActive(true);
                        blockPosition.x += 12;
                        tileType = 0;
                        break;
                    }
                }
                break;
            case 5:
                // Path
                for (int i = 0; i < prisonPaths.Count; i++)
                {
                    if (!prisonPaths[i].activeInHierarchy)
                    {
                        prisonPaths[i].transform.position = blockPosition;
                        prisonPaths[i].transform.rotation = rotation;
                        prisonPaths[i].SetActive(true);
                        blockPosition.x += 12;
                        tileType = 0;
                        break;
                    }
                }
                break;
            case 6:
                // Path
                for (int i = 0; i < prisonPaths.Count; i++)
                {
                    if (!prisonPaths[i].activeInHierarchy)
                    {
                        prisonPaths[i].transform.position = blockPosition;
                        prisonPaths[i].transform.rotation = rotation;
                        prisonPaths[i].SetActive(true);
                        blockPosition.x += 12;
                        tileType = 0;
                        break;
                    }
                }
                break;
            case 7:
                // Path
                for (int i = 0; i < prisonPaths.Count; i++)
                {
                    if (!prisonPaths[i].activeInHierarchy)
                    {
                        prisonPaths[i].transform.position = blockPosition;
                        prisonPaths[i].transform.rotation = rotation;
                        prisonPaths[i].SetActive(true);
                        blockPosition.x += 12;
                        tileType = 0;
                        break;
                    }
                }
                break;
        }
        GameObject.FindGameObjectWithTag("GameController").GetComponent<EnviromentChange>().counter += 1;
        blockPosition = new Vector3((int)blockPosition.x, (int)blockPosition.y, (int)blockPosition.z);
    }

    void RoofZone()
    {
        switch (roofTileNum())
        {
            case 1:
                // Door
                if (!roofObstacles[0].activeInHierarchy)
                {
                    roofObstacles[0].transform.position = new Vector3((int)blockPosition.x, 5.0135f, (int)blockPosition.z);
                    roofObstacles[0].transform.rotation = rotation2;
                    roofObstacles[0].SetActive(true);
                    blockPosition.x += 12;
                    tileType = 1;
                    break;
                }
                else if (!roofObstacles[1].activeInHierarchy)
                {
                    roofObstacles[1].transform.position = new Vector3((int)blockPosition.x, 5.0135f, (int)blockPosition.z);
                    roofObstacles[1].transform.rotation = rotation2;
                    roofObstacles[1].SetActive(true);
                    blockPosition.x += 12;
                    tileType = 1;
                    break;
                }
                break;
            case 2:
                // Pit
                if (!roofObstacles[2].activeInHierarchy)
                {
                    roofObstacles[2].transform.position = blockPosition;
                    roofObstacles[2].transform.rotation = rotation2;
                    roofObstacles[2].SetActive(true);
                    blockPosition.x += 12;
                    tileType = 1;
                    break;
                }
                else if (!prisonObstacles[3].activeInHierarchy)
                {
                    roofObstacles[3].transform.position = blockPosition;
                    roofObstacles[3].transform.rotation = rotation2;
                    roofObstacles[3].SetActive(true);
                    blockPosition.x += 12;
                    tileType = 1;
                    break;
                }
                break;
            case 3:
                // Wall
                if (!roofObstacles[4].activeInHierarchy)
                {
                    roofObstacles[4].transform.position = blockPosition;
                    roofObstacles[4].transform.rotation = rotation2;
                    roofObstacles[4].SetActive(true);
                    blockPosition.x += 12;
                    tileType = 1;
                    break;
                }
                else if (!prisonObstacles[5].activeInHierarchy)
                {
                    roofObstacles[5].transform.position = blockPosition;
                    roofObstacles[5].transform.rotation = rotation2;
                    roofObstacles[5].SetActive(true);
                    blockPosition.x += 12;
                    tileType = 1;
                    break;
                }
                break;
            case 4:
                // Path
                for (int i = 0; i < roofPaths.Count; i++)
                {
                    if (!roofPaths[i].activeInHierarchy)
                    {
                        roofPaths[i].transform.position = blockPosition;
                        roofPaths[i].transform.rotation = rotation2;
                        roofPaths[i].SetActive(true);
                        blockPosition.x += 12;
                        tileType = 0;
                        break;
                    }
                }
                break;
            case 5:
                // Path
                for (int i = 0; i < roofPaths.Count; i++)
                {
                    if (!roofPaths[i].activeInHierarchy)
                    {
                        roofPaths[i].transform.position = blockPosition;
                        roofPaths[i].transform.rotation = rotation2;
                        roofPaths[i].SetActive(true);
                        blockPosition.x += 12;
                        tileType = 0;
                        break;
                    }
                }
                break;
            case 6:
                // Path
                for (int i = 0; i < roofPaths.Count; i++)
                {
                    if (!roofPaths[i].activeInHierarchy)
                    {
                        roofPaths[i].transform.position = blockPosition;
                        roofPaths[i].transform.rotation = rotation2;
                        roofPaths[i].SetActive(true);
                        blockPosition.x += 12;
                        tileType = 0;
                        break;
                    }
                }
                break;
            case 7:
                // Path
                for (int i = 0; i < roofPaths.Count; i++)
                {
                    if (!roofPaths[i].activeInHierarchy)
                    {
                        roofPaths[i].transform.position = blockPosition;
                        roofPaths[i].transform.rotation = rotation2;
                        roofPaths[i].SetActive(true);
                        blockPosition.x += 12;
                        tileType = 0;
                        break;
                    }
                }
                break;
        }
        GameObject.FindGameObjectWithTag("GameController").GetComponent<EnviromentChange>().counter += 1;
        blockPosition = new Vector3((int)blockPosition.x, (int)blockPosition.y, (int)blockPosition.z);
    }

    void SewerZone()
    {
        switch (sewerTileNum())
        {
            case 1:
                // Door
                if (!sewerObstacles[0].activeInHierarchy)
                {
                    sewerObstacles[0].transform.position = blockPosition;
                    sewerObstacles[0].transform.rotation = rotation2;
                    sewerObstacles[0].SetActive(true);
                    blockPosition.x += 12;
                    tileType = 1;
                    break;
                }
                else if (!sewerObstacles[1].activeInHierarchy)
                {
                    sewerObstacles[1].transform.position = blockPosition;
                    sewerObstacles[1].transform.rotation = rotation2;
                    sewerObstacles[1].SetActive(true);
                    blockPosition.x += 12;
                    tileType = 1;
                    break;
                }
                break;
            case 2:
                // Pit
                if (!sewerObstacles[2].activeInHierarchy)
                {
                    sewerObstacles[2].transform.position = blockPosition;
                    sewerObstacles[2].transform.rotation = rotation2;
                    sewerObstacles[2].SetActive(true);
                    blockPosition.x += 12;
                    tileType = 1;
                    break;
                }
                else if (!sewerObstacles[3].activeInHierarchy)
                {
                    sewerObstacles[3].transform.position = blockPosition;
                    sewerObstacles[3].transform.rotation = rotation2;
                    sewerObstacles[3].SetActive(true);
                    blockPosition.x += 12;
                    tileType = 1;
                    break;
                }
                break;
            case 3:
                // Wall
                if (!sewerObstacles[4].activeInHierarchy)
                {
                    sewerObstacles[4].transform.position = blockPosition;
                    sewerObstacles[4].transform.rotation = rotation2;
                    sewerObstacles[4].SetActive(true);
                    blockPosition.x += 12;
                    tileType = 1;
                    break;
                }
                else if (!sewerObstacles[5].activeInHierarchy)
                {
                    sewerObstacles[5].transform.position = blockPosition;
                    sewerObstacles[5].transform.rotation = rotation2;
                    sewerObstacles[5].SetActive(true);
                    blockPosition.x += 12;
                    tileType = 1;
                    break;
                }
                break;
            case 4:
                // Path
                for (int i = 0; i < sewerPaths.Count; i++)
                {
                    if (!sewerPaths[i].activeInHierarchy)
                    {
                        sewerPaths[i].transform.position = blockPosition;
                        sewerPaths[i].transform.rotation = rotation2;
                        sewerPaths[i].SetActive(true);
                        blockPosition.x += 12;
                        tileType = 0;
                        break;
                    }
                }
                break;
            case 5:
                // Path
                for (int i = 0; i < sewerPaths.Count; i++)
                {
                    if (!sewerPaths[i].activeInHierarchy)
                    {
                        sewerPaths[i].transform.position = blockPosition;
                        sewerPaths[i].transform.rotation = rotation2;
                        sewerPaths[i].SetActive(true);
                        blockPosition.x += 12;
                        tileType = 0;
                        break;
                    }
                }
                break;
            case 6:
                // Path
                for (int i = 0; i < sewerPaths.Count; i++)
                {
                    if (!sewerPaths[i].activeInHierarchy)
                    {
                        sewerPaths[i].transform.position = blockPosition;
                        sewerPaths[i].transform.rotation = rotation2;
                        sewerPaths[i].SetActive(true);
                        blockPosition.x += 12;
                        tileType = 0;
                        break;
                    }
                }
                break;
            case 7:
                // Path
                for (int i = 0; i < sewerPaths.Count; i++)
                {
                    if (!sewerPaths[i].activeInHierarchy)
                    {
                        sewerPaths[i].transform.position = blockPosition;
                        sewerPaths[i].transform.rotation = rotation2;
                        sewerPaths[i].SetActive(true);
                        blockPosition.x += 12;
                        tileType = 0;
                        break;
                    }
                }
                break;
        }
        GameObject.FindGameObjectWithTag("GameController").GetComponent<EnviromentChange>().counter += 1;
        blockPosition = new Vector3((int)blockPosition.x, (int)blockPosition.y, (int)blockPosition.z);
    }
}