﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CanvasManager : MonoBehaviour {

    public GameObject menuCanvas;
    public GameObject creditsCanvas;
	public GameObject highScoreCanvas;
    public GameObject inGameUICanvas = null;
    public GameObject pauseCanvas;
    public GameObject gameOverCanvas;

    private GameObject activeCanvas;
    
    private GameObject gameManager;
    private GameObject C1;
    private GameObject C2;
    private GameObject C3;
    
    void Start () {
	    
		creditsCanvas.SetActive (false);

        // In game canvas
        inGameUICanvas.SetActive(true);
        pauseCanvas.SetActive(false);
        gameOverCanvas.SetActive(false);
        menuCanvas.SetActive(false);
        highScoreCanvas.SetActive(false);

        activeCanvas = inGameUICanvas;

        gameManager = GameObject.FindGameObjectWithTag("GameController");
        C1 = GameObject.FindGameObjectWithTag("Builder");
        C2 = GameObject.FindGameObjectWithTag("Hacker");
        C3 = GameObject.FindGameObjectWithTag("Tank");
    }

    void Update()
    {
        if (C1.GetComponent<CharacterSelect>().Alive == false || C2.GetComponent<CharacterSelect>().Alive == false || C3.GetComponent<CharacterSelect>().Alive == false)
        {
            gameOverCanvas.SetActive(true);
            Time.timeScale = 0;
            C1.GetComponent<Collider>().enabled = false;
            C2.GetComponent<Collider>().enabled = false;
            C3.GetComponent<Collider>().enabled = false;
        }
    }


    private void ChangeCanvas (GameObject canvasToActivate){

		if (activeCanvas != canvasToActivate) {
			activeCanvas.SetActive (false);
			activeCanvas = canvasToActivate;
			activeCanvas.SetActive (true);
		}
	}

	public void ChangeToMenu () {
		ChangeCanvas (menuCanvas);
        
        Time.timeScale = 0;
        gameManager.GetComponent<Speed>().paused = true;
        C1.GetComponent<CharacterSelect>().paused = true;
        C2.GetComponent<CharacterSelect>().paused = true;
        C3.GetComponent<CharacterSelect>().paused = true;
    }

	public void ChangeToCredits (){
		ChangeCanvas (creditsCanvas);
        
        Time.timeScale = 0;
        gameManager.GetComponent<Speed>().paused = true;
        C1.GetComponent<CharacterSelect>().paused = true;
        C2.GetComponent<CharacterSelect>().paused = true;
        C3.GetComponent<CharacterSelect>().paused = true;
    }
    public void ChangeToHighScore (){
        ChangeCanvas(highScoreCanvas);
        
        Time.timeScale = 0;
        gameManager.GetComponent<Speed>().paused = true;
        C1.GetComponent<CharacterSelect>().paused = true;
        C2.GetComponent<CharacterSelect>().paused = true;
        C3.GetComponent<CharacterSelect>().paused = true;
    }
    public void ChangeToPause (){
        ChangeCanvas(pauseCanvas);
        
        Time.timeScale = 0;
        gameManager.GetComponent<Speed>().paused = true;
        C1.GetComponent<CharacterSelect>().paused = true;
        C2.GetComponent<CharacterSelect>().paused = true;
        C3.GetComponent<CharacterSelect>().paused = true;

    }
    public void ChangeToInGameUI() {
        ChangeCanvas(inGameUICanvas);
        
        Time.timeScale = 1;
        gameManager.GetComponent<Speed>().paused = false;
        C1.GetComponent<CharacterSelect>().paused = false;
        C2.GetComponent<CharacterSelect>().paused = false;
        C3.GetComponent<CharacterSelect>().paused = false;
    }
    
    public void ChangeToGameOver() {
        ChangeCanvas(gameOverCanvas);
        
        Time.timeScale = 0;
        gameManager.GetComponent<Speed>().paused = true;
        C1.GetComponent<CharacterSelect>().paused = true;
        C2.GetComponent<CharacterSelect>().paused = true;
        C3.GetComponent<CharacterSelect>().paused = true;
    }

    public void ReloadScene()
    {
        SceneManager.LoadScene("main");
    }
}
