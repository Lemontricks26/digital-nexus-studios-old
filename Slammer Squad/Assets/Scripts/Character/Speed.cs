﻿using UnityEngine;
using System.Collections;

public class Speed : MonoBehaviour {

    [SerializeField]
    float acceleration;
    public bool paused = false;
    public bool tutorial = true;

    [SerializeField]
    CharacterSelect c1;
    [SerializeField]
    CharacterSelect c2;
    [SerializeField]
    CharacterSelect c3;
    [SerializeField]
    GameObject main;
    [SerializeField]
    GameObject chaser;

    [SerializeField]
    private float speedChar;
    private float a = 0.02f;
    [SerializeField]
    private float x = 0;
    private float b = 2;
    private float c = 3;
    private float d = 0.2f;

    private float deltaTime = 0.0f;
    private int stage = 1;
    
    void Start()
    {
        if (GetComponent<first_Time>().getHasPlayed() == 1)
        {
            stage = 2;
            x = 0.29f;
            main.transform.position = new Vector3(-31, main.transform.position.y, main.transform.position.z);
        }
        else if(GetComponent<first_Time>().getHasPlayed() == 0)
        {
            main.transform.position = new Vector3(-160, main.transform.position.y, main.transform.position.z);
        }
    }

    // https://www.desmos.com/calculator
    // y = a(x - b)^c + d
    // y = 0.02(x - 2)^3 + 0.2
    // y = (a * Mathf.Pow((x - b), c)) + d

    void Update()
    {
        deltaTime += (Time.deltaTime - deltaTime) * 0.1f;

        switch (stage)
        {
            case 1:
                speedChar = 0.1f;
                if (x >= 0.29f)
                {
                    stage = 2;
                    tutorial = false;
                }
                break;
            case 2:
                speedChar = (a * Mathf.Pow((x - b), c)) + d;
                if (x >= 4.145f)
                {
                    stage = 3;
                }
                break;
            case 3:
                speedChar = 0.4f;
                break;
            case 4:
                speedChar = 0;
                break;
        }
        if (!paused)
        {
            main.transform.position = Vector3.MoveTowards(main.transform.position, new Vector3(chaser.transform.position.x, main.transform.position.y, main.transform.position.z), speedChar);
            x += acceleration;
        }
        if (c1.Alive == false || c2.Alive == false || c3.Alive == false)
        {
            stage = 4;
        }
    }

    void OnGUI()
    {
        if (!paused)
        {
            int w = Screen.width, h = Screen.height;
    
            GUIStyle style = new GUIStyle();
    
            Rect rect = new Rect(0, 0, w, h * 2 / 100);
            style.alignment = TextAnchor.UpperRight;
            style.fontSize = h * 2 / 100;
            style.normal.textColor = new Color(1.0f, 1.0f, 1.5f, 1.0f);
            float msec = deltaTime * 1000.0f;
            float fps = 1.0f / deltaTime;
            string text = string.Format("{0:0.0} ms ({1:0.} fps)", msec, fps);
            GUI.Label(rect, text, style);
        }
    }
}
