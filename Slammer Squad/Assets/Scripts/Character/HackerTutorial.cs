﻿using UnityEngine;
using System.Collections;

public class HackerTutorial : MonoBehaviour {

    public GameObject marker;

	void Start () {
        GetComponent<Collider>().enabled = false;
	}
	
	void Update () {
	    if(this.gameObject.transform.position.x <= marker.transform.position.x)
        {
            GetComponent<Collider>().enabled = true;
            GetComponent<CharacterSelect>().move = true;
            GetComponent<CharacterSelect>().tutorial = false;
            GetComponent<HackerTutorial>().enabled = false;
        }
	}
}