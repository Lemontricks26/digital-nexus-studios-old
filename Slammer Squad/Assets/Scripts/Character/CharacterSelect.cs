﻿using UnityEngine;
using System.Collections;

public class CharacterSelect : MonoBehaviour {

    public bool Alive = true;
    public CharacterSelect char1, char2;
    public GameObject mark1, mark2, mark3;
    public first_Time gameManager;
    public bool selected = false;
    public int markerSet;
    public float speed = 1;

    public bool move = false;
    private float step;
    
    private float Speed = 0.1f;
    public bool paused = false;
    public bool tutorial = true;

    private float temp;

    private Collider coll;

    void Start()
    {
        coll = GetComponent<Collider>();

        if(gameManager.getHasPlayed() == 1)
        {
            if (tag == "Builder")
            {
                markerSet = 3;
                transform.position = mark3.transform.position;
            }
            if (tag == "Hacker")
            {
                markerSet = 2;
                transform.position = mark2.transform.position;
            }
            if (tag == "Tank")
            {
                markerSet = 1;
                transform.position = mark1.transform.position;
            }
            move = true;
            tutorial = false;
        }
        else if(gameManager.getHasPlayed() == 0)
        {
            if (tag == "Builder")
            {
                move = true;
                markerSet = 1;
            }
            if(tag == "Hacker")
            {
                markerSet = 2;
            }
            if(tag == "Tank")
            {
                markerSet = 3;
            }
        }
        Physics.IgnoreCollision(GetComponent<Collider>(), char1.GetComponent<Collider>());
        Physics.IgnoreCollision(GetComponent<Collider>(), char2.GetComponent<Collider>());
        Alive = true;

        temp = transform.position.x;
    }

    void Update()
    {
        if (move == true)
        {
            step = speed * Time.deltaTime;
            switch (markerSet)
            {
                case 1:
                    transform.position = Vector3.MoveTowards(transform.position, mark1.transform.position, step);
                    break;
                case 2:
                    transform.position = Vector3.MoveTowards(transform.position, mark2.transform.position, step);
                    break;
                case 3:
                    transform.position = Vector3.MoveTowards(transform.position, mark3.transform.position, step);
                    break;
            }
        }
        else if(move == false)
        {
            transform.position = Vector3.MoveTowards(transform.position, new Vector3(temp, transform.position.y, transform.position.z), Speed * 1.3f);
        }

#if UNITY_ANDROID || UNITY_IPHONE
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
            RaycastHit hit;

            if (coll.Raycast(ray, out hit, 100.0f) && hit.transform.gameObject.name == this.gameObject.name)
            {
                if (!paused)
                {
                    if (markerSet == 1)
                    {
                        if (transform.position.x >= mark1.transform.position.x || transform.position.x <= mark1.transform.position.x)
                        {
                            transform.position = mark1.transform.position;
                        }
                    }
                    if (markerSet == 2)
                    {
                        if (transform.position.x >= mark2.transform.position.x)
                        {
                            transform.position = mark1.transform.position;
                            markerSet = 1;

                            if (char1.markerSet == 1)
                            {
                                char1.markerSet = 2;
                            }
                            else if (char2.markerSet == 1)
                            {
                                char2.markerSet = 2;
                            }
                        }
                        else
                        {
                            transform.position = mark2.transform.position;
                            markerSet = 2;
                        }
                    }
                    if (markerSet == 3)
                    {
                        if (transform.position.x >= mark3.transform.position.x)
                        {
                            transform.position = mark1.transform.position;
                            markerSet = 1;

                            if (char1.markerSet == 1)
                            {
                                char1.markerSet = 3;
                            }
                            else if (char2.markerSet == 1)
                            {
                                char2.markerSet = 3;
                            }
                        }
                        else
                        {
                            transform.position = mark3.transform.position;
                            markerSet = 3;
                        }
                    }
                }
            }
        }
#endif
    }


#if UNITY_EDITOR || UNITY_STANDALONE_WIN
    void OnMouseDown()
    {
        if (!paused)
        {
        	if (markerSet == 1)
        	{
        		if (transform.position.x >= mark1.transform.position.x || transform.position.x <= mark1.transform.position.x)
        		{
        			transform.position = mark1.transform.position;
        		}
        	}
        	if (markerSet == 2)
        	{
        		if (transform.position.x >= mark2.transform.position.x)
        		{
        			transform.position = mark1.transform.position;
        			markerSet = 1;
        
        			if (char1.markerSet == 1)
        			{
                        char1.markerSet = 2;
        			}
        			else if (char2.markerSet == 1)
        			{
                        char2.markerSet = 2;
        			}
        		}
        		else
        		{
        			transform.position = mark2.transform.position;
        			markerSet = 2;
        		}
        	}
        	if (markerSet == 3)
        	{
        		if (transform.position.x >= mark3.transform.position.x)
        		{
        			transform.position = mark1.transform.position;
        			markerSet = 1;
        
        			if (char1.markerSet == 1)
        			{
                        char1.markerSet = 3;
        			}
        			else if (char2.markerSet == 1)
        			{
                        char2.markerSet = 3;
        			}
        		}
        		else
        		{
        			transform.position = mark3.transform.position;
        			markerSet = 3;
        		}
        	}
        }
    }
#endif

    void OnTriggerEnter(Collider col)
    {
        if(col.gameObject.tag == "DeathCheck")
        {
            Alive = false;
        }
    }
}