﻿using UnityEngine;
using System.Collections;

public class GuardControls : MonoBehaviour {
    public GameObject playerMarker;
    public GameObject backMarker;
    public GameObject standbyMarker;
    public GameObject standbyMarker_two;
    public GameObject guardWarning;

    public float forwardSpeed = 1;
    public float backwardsSpeed = 1;
    public float timer = 1.5f;
    public int num = 1;
    float Rtime;

    Animator anim;

    private Collider coll;

    void Start () {
        coll = GetComponent<Collider>();

        transform.position = backMarker.transform.position;
        anim = transform.GetChild(0).GetComponent<Animator>();
    }

    void Update()
    {

        switch (num)
        {
            case 1:
                forwardMovement();

                break;
            case 2:
                reverseMovement();
                break;
            case 3:
                standbye();
                break;
        }

        if ((int)transform.position.x >= (int)standbyMarker_two.transform.position.x)
        {
            guardWarning.SetActive(true);
        }
        else
        {
            guardWarning.SetActive(false);
        }

#if UNITY_ANDROID || UNITY_IPHONE
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
            RaycastHit hit;

            if (coll.Raycast(ray, out hit, 100.0f) && hit.transform.gameObject.name == this.gameObject.name)
            {
                timer = 1.5f;
                num = 2;

                anim.SetBool("GOT CUCKT", true);
            }
        }
#endif
    }

    void forwardMovement()
    {
        anim.SetBool("GOT CUCKT", false);
        float step = forwardSpeed * Time.deltaTime;

        transform.position = Vector3.MoveTowards(transform.position, new Vector3(standbyMarker.transform.position.x, 8, standbyMarker.transform.position.z), step);

            if (transform.position.x >= standbyMarker.transform.position.x)
            {
                timer -= Time.deltaTime;
            }
        if (timer <= 0)
        {
            num = 3;
        }
    }

    void reverseMovement()
    {
        float step = backwardsSpeed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, new Vector3(backMarker.transform.position.x, 8, backMarker.transform.position.z), step * 2);
        if(transform.position.x <= backMarker.transform.position.x)
        {
            Rtime = Random.Range(0, 50);
            Rtime -= Time.deltaTime;
            if(Rtime <= 0)
            {
                num = 1;
            }
        }
    }

#if UNITY_EDITOR || UNITY_STANDALONE_WIN
    void OnMouseDown()
    {
        timer = 1.5f;
        num = 2;
    
        anim.SetBool("GOT CUCKT", true);
    }
#endif

    void OnTriggerEnter(Collider col)
    {
        if(col.gameObject.tag == "Builder" || col.gameObject.tag == "Tank" || col.gameObject.tag == "Hacker")
        {
            col.gameObject.GetComponent<CharacterSelect>().Alive = false;
        }
    }

    void standbye()
    {
        float step = forwardSpeed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, new Vector3(playerMarker.transform.position.x, 8, playerMarker.transform.position.z), step);
    }
}
