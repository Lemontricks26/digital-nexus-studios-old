using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class splash : MonoBehaviour {

	public Image splashImage;
    public Image backGround;

	private float timeToFade = 0;
    
	IEnumerator Start () 
	{
		yield return new WaitForSeconds(4f);
        Destroy(splashImage);
        Destroy(backGround);
	}
	
	void Update () 
	{
		timeToFade += Time.deltaTime;

		if (timeToFade >= 2.0f)
			splashImage.CrossFadeAlpha (0f, 1f, false);
	}
}